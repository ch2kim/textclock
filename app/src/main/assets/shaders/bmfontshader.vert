uniform vec4 u_uvtranslate;

void main()
{
	gl_Position = CC_MVPMatrix * a_position;
	
	vColor = a_color;
	vTexcoord = a_texCoord.xy + u_uvtranslate.xy;
}