package com.alongthecloud.app.textclock;

import android.content.res.AssetManager;

/**
 * Created by Na on 2017-10-01.
 */

public class JniLib {
    static {
        System.loadLibrary("hello-jni");
    }

    //public static native void create();
    public static native void destroy();

    public static native int init();
    public static native void changed(int width, int height);
    public static native void update(long deltaTime);

    public static native void setupAssetManager(AssetManager amgr);
    public static native void message(String name, String param0, int param1, int param2);
}
