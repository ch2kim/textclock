#ifndef OGL_FILEMANAGER_H
#define OGL_FILEMANAGER_H

#include "system/DataBuffer.h"

namespace OGL
{
	class FileManager
	{
	public:
		static FileManager* getInstance();
		const char* getWriteablePath();
		void setAssetManager(void* assetManager);

		bool loadFromAsset(DataBuffer& buffer, const char* filename, bool binary = true);
		bool loadFromAsset(void** buffer, long* size, const char* filename, bool binary = true);

		void saveToData(const char* filename, DataBuffer& buffer);

	protected:
		FileManager();
		~FileManager();

		void* _assetManager;
	};
};

#endif