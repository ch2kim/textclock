#ifndef _BMFONT_H_
#define _BMFONT_H_

#include "dry.h"
#include <base/StdMacro.hpp>

#include <vector>
#include <map>
#include <string>

namespace OGL
{
	class BMFont
	{
	public:
		BMFont();
		~BMFont();

		bool initWithFile(const char* fontfileName);

		int getSize() { return _size; }
		int getHeight() { return _height; }
		
		float getScaleW() { return _scaleW; }
		float getScaleH() { return _scaleH; }

		int getPageCount() { return _pageCount; }

		dry::Texture* getTexture(int index) { return _fontTextures[index]; }

		struct BMChar
		{
			int id;
			int page;

			int x, y;
			int w, h;

			int offsetX, offsetY;
			int advance;
		};

		BMChar* findFont(int cid);

	private:

		std::map<int, BMChar> _fontChars;
		std::vector<dry::Texture*> _fontTextures;

		int _size;
		int _height;
		int _base;
		float _scaleW;
		float _scaleH;
		
		int _pageCount;

		void destroy();
	};
};


#endif