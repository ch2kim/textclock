uniform vec4 u_color;

void main()
{
	vec4 tex = texture2D(CC_Texture0, vTexcoord);
	gl_FragColor = tex * u_color;

}