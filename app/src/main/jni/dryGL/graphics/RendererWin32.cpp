#include "dry.h"
#include "Renderer.h"

using namespace dry;

#ifdef WIN32

// PowerVR OpenGL ES Emulator & that code
#include <tchar.h>

bool TestEGLError(HWND hWnd, TCHAR* ptszLocation)
{
	/*
	eglGetError returns the last error that has happened using egl,
	not the status of the last called function. The user has to
	check after every single egl call or at least once every frame.
	*/
	EGLint iErr = eglGetError();
	if (iErr != EGL_SUCCESS)
	{
#ifndef NO_GDI
		TCHAR pszStr[256];
		_stprintf(pszStr, _T("%s failed (%d).\n"), ptszLocation, iErr);
		MessageBox(hWnd, pszStr, _T("Error"), MB_OK | MB_ICONEXCLAMATION);
#endif
		return false;
	}

	return true;
}

bool Renderer::InitContext(HWND hWnd, bool depth, bool stencil)
{
	EGLConfig			eglConfig = 0;
	EGLNativeWindowType	eglWindow = 0;
	HDC hDC = 0;
	
	eglWindow = hWnd;
	hDC = GetDC(hWnd);

	eglDisplay = eglGetDisplay(hDC);
	if (eglDisplay == EGL_NO_DISPLAY)
	{
		eglDisplay = eglGetDisplay((EGLNativeDisplayType)EGL_DEFAULT_DISPLAY);
	}

	EGLint iMajorVersion, iMinorVersion;
	if (!eglInitialize(eglDisplay, &iMajorVersion, &iMinorVersion))
	{
#ifndef NO_GDI
		MessageBox(0, _T("eglInitialize() failed."), _T("Error"), MB_OK | MB_ICONEXCLAMATION);
#endif
		return false;
	}

	const EGLint pi32ConfigAttribs[] =
	{
		EGL_LEVEL,				0,
		EGL_SURFACE_TYPE,		EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE,	EGL_OPENGL_ES2_BIT,
		EGL_NATIVE_RENDERABLE,	EGL_FALSE,
		EGL_DEPTH_SIZE,			EGL_DONT_CARE,
		EGL_NONE
	};

	int iConfigs;
	if (!eglChooseConfig(eglDisplay, pi32ConfigAttribs, &eglConfig, 1, &iConfigs) || (iConfigs != 1))
	{
#ifndef NO_GDI
		MessageBox(0, _T("eglChooseConfig() failed."), _T("Error"), MB_OK | MB_ICONEXCLAMATION);
#endif
		return false;
	}

	eglSurface = eglCreateWindowSurface(eglDisplay, eglConfig, eglWindow, NULL);

	if (eglSurface == EGL_NO_SURFACE)
	{
		eglGetError(); // Clear error
		eglSurface = eglCreateWindowSurface(eglDisplay, eglConfig, NULL, NULL);
	}

	if (!TestEGLError(hWnd, _T("eglCreateWindowSurface")) )
	{
		return false;
	}

	eglBindAPI(EGL_OPENGL_ES_API);
	EGLint ai32ContextAttribs[] = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE };
	eglContext = eglCreateContext(eglDisplay, eglConfig, NULL, ai32ContextAttribs);

	if (!TestEGLError(hWnd, _T("eglCreateContext")) )
	{
		return false;
	}

	eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext);
	if (!TestEGLError(hWnd, _T("eglMakeCurrent")) )
	{
		return false;
	}

	this->m_hDC = hDC;
	this->m_hWnd = hWnd;
	this->m_owned = true;

	return true;
}

void Renderer::FreeContext()
{
	if (eglDisplay)
	{
		eglMakeCurrent(eglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
		eglTerminate(eglDisplay);
		eglDisplay = 0;
	}

	if (m_hDC)
	{
		ReleaseDC(m_hWnd, m_hDC);

		m_hDC = NULL;
		m_hWnd = NULL;
	}
}

#endif