#include <engine/ShaderManager.hpp>
#include <base/StdMacro.hpp>

#include <system/DataBuffer.h>
#include <system/FileManager.h>

/*
GLSL preprocess

#define
#undef
#if
#ifdef
#ifndef
#else
#elif
#endif
*/

namespace OGL
{
	ShaderManager* ShaderManager::_instance = NULL;

	void ShaderManager::init()
	{
		if (!_instance)
		{
			_instance = new ShaderManager();
			_instance->initProgram();
		}
	}

	void ShaderManager::quit()
	{
		if (_instance)
		{
			_instance->releaseProgram();
			delete _instance;
			_instance = NULL;
		}
	}

	ShaderManager::ShaderManager()
	{
		_vertAttrTbl[VertexFlag::VF_POSITION] = "attribute vec4 a_position";
		_vertAttrTbl[VertexFlag::VF_NORMAL] = "attribute vec3 a_normal";

#ifdef OOGL_PLATFORM_GLES
		_vertAttrTbl[VertexFlag::VF_COLOR] = "attribute lowp vec4 a_color";
#else
		_vertAttrTbl[VertexFlag::VF_COLOR] = "attribute vec4 a_color";
#endif
		_vertAttrTbl[VertexFlag::VF_UV0] = "attribute vec2 a_texCoord";
		_vertAttrTbl[VertexFlag::VF_UV1] = "attribute vec2 a_texCoord1";
		_vertAttrTbl[VertexFlag::VF_UV2] = "attribute vec2 a_texCoord2";

		_vertAttrTbl[VertexFlag::VF_UV0x] = "attribute vec4 a_texCoord";
		_vertAttrTbl[VertexFlag::VF_UV1x] = "attribute vec4 a_texCoord1";
		_vertAttrTbl[VertexFlag::VF_UV2x] = "attribute vec4 a_texCoord2";

#ifdef OOGL_PLATFORM_GLES
		_varyingTbl[VertexFlag::VF_NORMAL] = "varying lowp vec3 vNormal";
		_varyingTbl[VertexFlag::VF_COLOR] = "varying lowp vec4 vColor";

		_varyingTbl[VertexFlag::VF_UV0] = "varying mediump vec2 vTexcoord";
		_varyingTbl[VertexFlag::VF_UV1] = "varying mediump vec2 vTexcoord1";
		_varyingTbl[VertexFlag::VF_UV2] = "varying mediump vec2 vTexcoord2";

		_varyingTbl[VertexFlag::VF_UV0x] = "varying mediump vec4 vTexcoord";
		_varyingTbl[VertexFlag::VF_UV1x] = "varying mediump vec4 vTexcoord1";
		_varyingTbl[VertexFlag::VF_UV2x] = "varying mediump vec4 vTexcoord2";
#else
		_varyingTbl[VertexFlag::VF_NORMAL] = "varying vec3 vNormal";
		_varyingTbl[VertexFlag::VF_COLOR] = "varying vec4 vColor";

		_varyingTbl[VertexFlag::VF_UV0] = "varying vec2 vTexcoord";
		_varyingTbl[VertexFlag::VF_UV1] = "varying vec2 vTexcoord1";
		_varyingTbl[VertexFlag::VF_UV2] = "varying vec2 vTexcoord2";

		_varyingTbl[VertexFlag::VF_UV0x] = "varying vec4 vTexcoord";
		_varyingTbl[VertexFlag::VF_UV1x] = "varying vec4 vTexcoord1";
		_varyingTbl[VertexFlag::VF_UV2x] = "varying vec4 vTexcoord2";
#endif
	}

	ShaderManager::~ShaderManager()
	{
		_vertAttrTbl.clear();
		_varyingTbl.clear();
	}

#ifdef OOGL_PLATFORM_GLES
	static const char* COMMON_MACRO =
		"#define MAX_DIRECTIONAL_LIGHT_NUM 1\n"
		"#define OGLES\n"
		"precision mediump float;\n";

	static const char* COMMON_LIGHT_UNIFORM_DEFINE_VERTEX =
		"\n#if (MAX_DIRECTIONAL_LIGHT_NUM > 0)\n"
		"uniform mediump vec4 u_DirLightSourceColor[MAX_DIRECTIONAL_LIGHT_NUM];"
		"uniform mediump vec3 u_DirLightSourceDirection[MAX_DIRECTIONAL_LIGHT_NUM];"
		"\n#endif\n";

	static const char* COMMON_ADDITIONAL_UNIFORM =
		"uniform mat4 CC_WMatrix;\n"
		"uniform mediump vec3 u_CamPos;\n"

		"uniform mediump vec4 u_Color;\n"
		"uniform mediump float u_specularPow;\n"
		"uniform mediump vec4 u_Factor;\n";
#else
	static const char* COMMON_MACRO =
		"#define MAX_DIRECTIONAL_LIGHT_NUM 1\n";

	static const char* COMMON_LIGHT_UNIFORM_DEFINE_VERTEX =
		"\n#if (MAX_DIRECTIONAL_LIGHT_NUM > 0)\n"
		"uniform vec4 u_DirLightSourceColor[MAX_DIRECTIONAL_LIGHT_NUM];"
		"uniform vec3 u_DirLightSourceDirection[MAX_DIRECTIONAL_LIGHT_NUM];"
		"\n#endif\n";

	static const char* COMMON_ADDITIONAL_UNIFORM =
		"uniform mat4 CC_WMatrix;\n"
		"uniform vec3 u_CamPos;\n"

		"uniform vec4 u_Color;\n"
		"uniform float u_specularPow;\n"
		"uniform vec4 u_Factor;\n";
#endif
	/*
	Directional Light : Direction and Source (.a - intensity)
	u_Color = Diffuse Color
	u_specularPow = Specular Shiness (필요할 때만 사용)

	Vertex 의 color 는 각 셰이더마다 여러 파라미터로 사용.
	기본적으로 Directional Light 를 적용. 물 같은 특수 경우를 제외하고, HemiSphere Light 도 적용.
	*/

	void AppendCodeWithFlag(std::string& buf, const ShaderManager::STRINGTABLE& tbl, GLuint flags[])
	{
		for (int i = 0; flags[i] != 0; ++i)
		{
			auto it = tbl.find(flags[i]);
			if (it != tbl.end())
			{
				buf.append((*it).second);
				buf.append(";\n");
			}
		}
	}

	static const char* COMMON_UNIFORM_DEFINE_VERTEX =
		"uniform mat4 CC_PMatrix;\n"
		"uniform mat4 CC_MVMatrix;\n"
		"uniform mat4 CC_MVPMatrix;\n"
		"uniform mat3 CC_NormalMatrix;\n"
		"uniform vec4 CC_Time;\n"
		"uniform vec4 CC_SinTime;\n"
		"uniform vec4 CC_CosTime;\n"
		"uniform vec4 CC_Random01;\n";

	static const char* COMMON_UNIFORM_DEFINE_FRAGMENT =
		"uniform sampler2D CC_Texture0;\n"
		"uniform sampler2D CC_Texture1;\n"
		"uniform sampler2D CC_Texture2;\n"
		"uniform sampler2D CC_Texture3;\n";

	//CC INCLUDES END

	/*
	setUniformLocationWith4f([UNIFORM_TIME], time/10.0, time, time*2, time*4);
	setUniformLocationWith4f([UNIFORM_SIN_TIME], time/8.0, time/4.0, time/2.0, sinf(time));
	setUniformLocationWith4f([UNIFORM_COS_TIME], time/8.0, time/4.0, time/2.0, cosf(time));
	*/

	const char* lighting_function = STRING(
		vec3 computeLighting(vec3 normalVector, vec3 lightDirection, vec3 lightColor, float attenuation)
		{
			float diffuse = max(dot(normalVector, lightDirection), 0.0);
			vec3 diffuseColor = (lightColor  * diffuse * attenuation);
			return diffuseColor;
		}
	\n
	);

	bool getStringFromFile(std::string& outString, const char* filepath)
	{
		OGL::DataBuffer buffer;
		if (FileManager::getInstance()->loadFromAsset(buffer, filepath, false))
		{
			outString = (char*)buffer.getBufferPtr();
			return true;
		}

		dry::Log(dry::LogInfo, "shader open failed : %s\n", filepath);
		return false;
	}

	bool getShaderFromFile(const char* name, std::string& vertexShader, std::string& fragShader)
	{
		const char* SHADER_PATH = "shaders";
		
		char vertFilename[256];
		char fragFilename[256];

		sprintf(vertFilename, "%s/%s.vert", SHADER_PATH, name);
		sprintf(fragFilename, "%s/%s.frag", SHADER_PATH, name);

		if (!getStringFromFile(vertexShader, vertFilename))
			return false;

		if (!getStringFromFile(fragShader, fragFilename))
			return false;

		return true;
	}

	enum ShaderDefineFlag
	{
		SF_LIGHTING = 0x01,
		SF_ADDITIONAL = 0x02,
	};

	dry::Shader* loadShaderProgram(
		dry::Shader* program,
		const ShaderManager::STRINGTABLE& _vertAttrTbl,
		const ShaderManager::STRINGTABLE& _varyingTbl,
		const char* shaderName,
		GLuint* attribFlags,
		GLuint* varyingFlags,
		GLuint vertDefine,
		GLuint fragDefine)
	{
		std::string vertBuffer, fragBuffer;
		std::string varying;

		std::string vertData, fragData;
		if (!getShaderFromFile(shaderName, vertData, fragData))
			return nullptr;

		vertBuffer.append(COMMON_MACRO);
		vertBuffer.append(COMMON_UNIFORM_DEFINE_VERTEX);

		if (vertDefine & SF_LIGHTING) vertBuffer.append(COMMON_LIGHT_UNIFORM_DEFINE_VERTEX);
		if (vertDefine & SF_ADDITIONAL) vertBuffer.append(COMMON_ADDITIONAL_UNIFORM);

		fragBuffer.append(COMMON_MACRO);
		fragBuffer.append(COMMON_UNIFORM_DEFINE_FRAGMENT);

		if (fragDefine & SF_LIGHTING) fragBuffer.append(COMMON_LIGHT_UNIFORM_DEFINE_VERTEX);
		if (fragDefine & SF_ADDITIONAL) fragBuffer.append(COMMON_ADDITIONAL_UNIFORM);

		AppendCodeWithFlag(vertBuffer, _vertAttrTbl, attribFlags);
		AppendCodeWithFlag(varying, _varyingTbl, varyingFlags);

		vertBuffer.append(varying);
		if (vertDefine & SF_LIGHTING) vertBuffer.append(lighting_function);

		vertBuffer.append(vertData);

		fragBuffer.append(varying);
		if (fragDefine & SF_LIGHTING) fragBuffer.append(lighting_function);

		fragBuffer.append(fragData);

		_ASSERT(program);
		if (program->Init(vertBuffer.c_str(), fragBuffer.c_str()))
			return program;
		else
			return nullptr;
	}

	dry::Shader* getPosTexShader(
		dry::Shader* program,
		const ShaderManager::STRINGTABLE& _vertAttrTbl,
		const ShaderManager::STRINGTABLE& _varyingTbl
		)
	{
		GLuint attribFlags[] = { VertexFlag::VF_POSITION, VertexFlag::VF_UV0, 0 };
		GLuint varyingFlags[] = { VertexFlag::VF_UV0, 0 };

		return loadShaderProgram(program, _vertAttrTbl, _varyingTbl, "postex", attribFlags, varyingFlags, 0, 0);
	}

	dry::Shader* getPosTexUCShader(
		dry::Shader* program,
		const ShaderManager::STRINGTABLE& _vertAttrTbl,
		const ShaderManager::STRINGTABLE& _varyingTbl
		)
	{
		GLuint attribFlags[] = { VertexFlag::VF_POSITION, VertexFlag::VF_UV0, 0 };
		GLuint varyingFlags[] = { VertexFlag::VF_UV0, 0 };

		return loadShaderProgram(program, _vertAttrTbl, _varyingTbl, "postexuc", attribFlags, varyingFlags, 0, 0);
	}

	dry::Shader* getPosClrTexUCShader(
		dry::Shader* program,
		const ShaderManager::STRINGTABLE& _vertAttrTbl,
		const ShaderManager::STRINGTABLE& _varyingTbl
	)
	{
		GLuint attribFlags[] = { VertexFlag::VF_POSITION, VertexFlag::VF_COLOR, VertexFlag::VF_UV0, 0 };
		GLuint varyingFlags[] = { VertexFlag::VF_COLOR, VertexFlag::VF_UV0, 0 };

		return loadShaderProgram(program, _vertAttrTbl, _varyingTbl, "posclrtexuc", attribFlags, varyingFlags, 0, 0);
	}

	dry::Shader* getBMFontShader(
		dry::Shader* program,
		const ShaderManager::STRINGTABLE& _vertAttrTbl,
		const ShaderManager::STRINGTABLE& _varyingTbl
	)
	{
		GLuint attribFlags[] = { VertexFlag::VF_POSITION, VertexFlag::VF_COLOR, VertexFlag::VF_UV0, 0 };
		GLuint varyingFlags[] = { VertexFlag::VF_COLOR, VertexFlag::VF_UV0, 0 };

		return loadShaderProgram(program, _vertAttrTbl, _varyingTbl, "bmfontshader", attribFlags, varyingFlags, 0, 0);
	}

	dry::Shader* ShaderManager::getProgram(int shaderFlag)
	{
		auto iter = _programTable.find(shaderFlag);
		if (iter != _programTable.end())
		{
			return iter->second;
		}
		else
		{
			return nullptr;
		}
	}

	void ShaderManager::initProgram()
	{
		auto getShader = [this](int flag, dry::Shader* (func)(dry::Shader* shader, const ShaderManager::STRINGTABLE&, const ShaderManager::STRINGTABLE&))
		{
			dry::Shader* program = (*func)(new dry::Shader, _vertAttrTbl, _varyingTbl);
			if (!program)
				return false;

			_programTable[flag] = program;
			return true;
		};

		_VERIFY(getShader(ShaderFlag::BMFONT_SHADER, getBMFontShader));

		//_VERIFY(getShader(ShaderFlag::POSTEX_SHADER, getPosTexShader));
		
		_VERIFY(getShader(ShaderFlag::POSTEXUC_SHADER, getPosTexUCShader));

		//_VERIFY(getShader(ShaderFlag::POSCLRTEXUC_SHADER, getPosClrTexUCShader));

		//_VERIFY(getShader(ShaderFlag::POSCLR_SHADER, getPosClrShader));
		//_VERIFY(getShader(ShaderFlag::POSCLRNRM_SHADER, getPosClrNrmShader));
		//_VERIFY(getShader(ShaderFlag::POSCLRTEX_SHADER, getPosClrTexShader));
		//_VERIFY(getShader(ShaderFlag::POSNRMTEX_SHADER, getPosNrmTexShader));
		//_VERIFY(getShader(ShaderFlag::POSCLRNRMTEX_SHADER, getPosClrNrmTexShader));
	}

	void ShaderManager::releaseProgram()
	{
		for (auto it = _programTable.begin(); it != _programTable.end(); ++it)
		{
			dry::Shader* program = it->second;
			DISPOSE(program);
		}

		_programTable.clear();
	}
}