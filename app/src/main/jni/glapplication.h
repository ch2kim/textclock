//
// Created by CHKim on 2016-02-03.
//

#ifndef GLAPPLICATION_H
#define GLAPPLICATION_H

#include <base/StdMacro.hpp>
#include "platform.h"

#include "dry.h"
#include "graphics/Renderer.h"
#include "engine/Node.hpp"

namespace OGL
{
	class BMFont;
	class BMFontRender;
};

class GLApplication
{
public:
	GLApplication();
	~GLApplication();

	bool init(dry::Renderer* renderer);
	void resize(int width, int height);

	bool update(float dt);
	void render();

    void sendMessage(const char* msg, const char* param0 = 0, int param1 = 0, int param2 = 0);

private:
	void initScene(bool landscape);
	void initLayout(bool landscape);
	void clearScene();

	void destroy();

	std::vector<OGL::RenderNode*> _root;

	dry::CameraOrthogonal _cam;
	dry::Renderer* _renderer;

	float _time;
	float _deltaTime;

	bool _numericMode;
	bool _landscape;

	glm::vec3 _backgroundColor;

	OGL::BMFont* _bmfont;
	
	OGL::BMFontRender* _bmtext1;
	OGL::BMFontRender* _bmtext2;
};

#endif //GLAPPLICATION_H
