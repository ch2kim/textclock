#include "bmfont.h"

#include <system/FileManager.h>
#include <system/DataBuffer.h>
#include <engine/ShaderManager.hpp>

#include <sstream>
#include "pugixml.hpp"

namespace OGL
{
	BMFont::BMFont()
	{
		_size = 0;
		_height = 0;
		_base = 0;
		_scaleW = 0.0f;
		_scaleH = 0.0f;
		_pageCount = 0;
	}

	BMFont::~BMFont()
	{
		destroy();
	}

	void BMFont::destroy()
	{
		for (auto iter = _fontTextures.begin(); iter != _fontTextures.end(); ++iter)
		{
			DISPOSE(*iter);
		}

		_fontTextures.clear();
		_fontChars.clear();
	}

	bool BMFont::initWithFile(const char* fontfileName)
	{
		FileManager* fm = FileManager::getInstance();

		OGL::DataBuffer buffer;
		if (!fm->loadFromAsset(buffer, fontfileName, false))
			return false;

		pugi::xml_document doc;
		//pugi::xml_parse_result result = doc.load_buffer(buffer.getBufferPtr(), buffer.getBufferSize());
		pugi::xml_parse_result result = doc.load_buffer_inplace(buffer.getBufferPtr(), buffer.getBufferSize());

		pugi::xml_node fontNode = doc.child("font");
		pugi::xml_node infoNode = fontNode.child("info");

		int size = infoNode.attribute("size").as_int();
		if (size < 0)
			size = -size;

		bool unicode = infoNode.attribute("unicode").as_int() ? true : false;
		bool outline = infoNode.attribute("outline").as_int() ? true : false;

		pugi::xml_node commonNode = fontNode.child("common");
		int lineHeight = commonNode.attribute("lineHeight").as_int();
		int lineBase = commonNode.attribute("base").as_int();
		int pages = commonNode.attribute("pages").as_int();

		float scaleW = (float)commonNode.attribute("scaleW").as_int();
		float scaleH = (float)commonNode.attribute("scaleH").as_int();

		// 폰트 읽기
		pugi::xml_node pagesNode = fontNode.child("pages");
		for (pugi::xml_node pageNode = pagesNode.child("page"); pageNode; pageNode = pageNode.next_sibling("page"))
		{
			const char* filename = pageNode.attribute("file").as_string();

			dry::Texture::Params param;
			param.Bilinear = true;
			param.Mipmaps = false;
			param.Repeat = false;

			dry::Image image;
			if (image.InitWithAsset(filename))
			{
				dry::Texture* texture = new dry::Texture();
				if (!texture->InitWithImage(image, param))
				{
					delete texture;
					destroy();
					return false;
				}
				else
				{
					_fontTextures.push_back(texture);
				}
			}
		}

		// 글자 정보 읽기
		pugi::xml_node charNodes = fontNode.child("chars");
		int charCount = charNodes.attribute("count").as_int();
		for (pugi::xml_node charNode = charNodes.child("char"); charNode; charNode = charNode.next_sibling("char"))
		{
			BMChar bmchar;
			int id;
			id = charNode.attribute("id").as_int();
			bmchar.id = id;

			bmchar.x = charNode.attribute("x").as_int();
			bmchar.y = charNode.attribute("y").as_int();
			bmchar.w = charNode.attribute("width").as_int();
			bmchar.h = charNode.attribute("height").as_int();

			bmchar.offsetX = charNode.attribute("xoffset").as_int();
			bmchar.offsetY = charNode.attribute("yoffset").as_int();
			bmchar.advance = charNode.attribute("xadvance").as_int();
			bmchar.page = charNode.attribute("page").as_int();

			_fontChars[id] = bmchar;
		}

		this->_size = size;
		this->_height = lineHeight;
		this->_base = lineBase;
		this->_scaleW = scaleW;
		this->_scaleH = scaleH;
		this->_pageCount = pages;

		return true;
	}

	BMFont::BMChar* BMFont::findFont(int cid)
	{
		auto ci = _fontChars.find(cid);
		if (ci == _fontChars.end())
			return nullptr;

		return &ci->second;
	}
};