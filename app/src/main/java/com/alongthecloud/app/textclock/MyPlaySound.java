package com.alongthecloud.app.textclock;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.speech.tts.TextToSpeech;

import java.util.Locale;

/**
 * Created by Na on 2017-10-08.
 */

public class MyPlaySound {

    static public TextToSpeech tts = null;

    static private void Init(Context c) {
        if (tts == null) {
            tts = new TextToSpeech(c, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if(status != TextToSpeech.ERROR) {
                        tts.setLanguage(Locale.KOREAN);
                    }
                }
            });
        }
    }
    static public void PlayRingtone(Context c) {

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (notification != null) {
            Ringtone r = RingtoneManager.getRingtone(c, notification);
            if (r != null) {
                r.play();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    static public void PlayTTS(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (tts == null) {
                Init(c);
            }

            CalendarText ct = new CalendarText(true);
            String text = ct.text2;

            String utteranceId = c.hashCode() + "";
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
        }
    }
}
