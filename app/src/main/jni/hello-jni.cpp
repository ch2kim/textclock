#include "glapplication.h"

#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <system/FileManager.h>

GLApplication* app = nullptr;
dry::Renderer* renderer = nullptr;

void destroy()
{
    dry::Log(dry::LogInfo, "jni::destroy");

    DISPOSE(app);
    DISPOSE(renderer);
}

bool setupGraphics()
{
    DISPOSE(renderer);
    DISPOSE(app);

    renderer = new dry::Renderer();
    renderer->Init();
    dry::Log(dry::LogInfo, "jni::Renderer::Init");

    glm::ivec4 vp = renderer->GetViewport();
    // dry::Log(dry::LogInfo, "jni::setupGraphics view port [%d, %d, %d, %d]", vp.x, vp.y, vp.z, vp.w);

    app = new GLApplication();
    app->init(renderer);
    dry::Log(dry::LogInfo, "jni::GLApplication::Init");

    dry::Log(dry::LogInfo, "jni::setupGraphics");
    return true;
}

void updateGraphics(int w, int h)
{
    dry::Log(dry::LogInfo, "jni::updateGraphics (%d, %d)", w, h);

    if (app && renderer)
    {
        app->resize(w, h);
    }
}

jboolean renderFrame(float dt)
{
    // dry::Log(dry::LogInfo, "jni::renderFrame %3.2f", dt);
    if (app && renderer)
    {
        bool ret = app->update(dt);
        app->render();

        return ret ? 1 : 0;
    }

    return 0;
}

// package com.alongthecloud.app.textclock;
// Java_com_alongthecloud_app_textclock_JniLib_

extern "C" {
JNIEXPORT void JNICALL Java_com_alongthecloud_app_textclock_JniLib_create(JNIEnv *env,
                                                                             jobject obj);
JNIEXPORT void JNICALL Java_com_alongthecloud_app_textclock_JniLib_destroy(JNIEnv *env,
                                                                              jobject obj);

JNIEXPORT int JNICALL Java_com_alongthecloud_app_textclock_JniLib_init(JNIEnv * env, jobject obj);
JNIEXPORT void JNICALL Java_com_alongthecloud_app_textclock_JniLib_changed(JNIEnv * env, jobject obj, jint width, jint height);
JNIEXPORT void JNICALL Java_com_alongthecloud_app_textclock_JniLib_update(JNIEnv * env, jobject obj, jlong deltaTime);

JNIEXPORT void JNICALL Java_com_alongthecloud_app_textclock_JniLib_setupAssetManager(JNIEnv *env,
                                                                                                jobject obj,
                                                                                                jobject assetManager);

JNIEXPORT void JNICALL Java_com_alongthecloud_app_textclock_JniLib_message(JNIEnv *env,
                                                                       jobject obj,
                                                                       jstring name,
                                                                       jstring param0,
                                                                       jint param1,
                                                                       jint param2);
}

JNIEXPORT void JNICALL Java_com_alongthecloud_app_textclock_JniLib_create(JNIEnv * env, jobject obj) {
    //__android_log_print(ANDROID_LOG_INFO, "hello-jni", "Jni::create");
}

JNIEXPORT void JNICALL Java_com_alongthecloud_app_textclock_JniLib_destroy(JNIEnv * env, jobject obj) {
    //__android_log_print(ANDROID_LOG_INFO, "hello-jni", "Jni::destroy");
    destroy();
}

JNIEXPORT int JNICALL Java_com_alongthecloud_app_textclock_JniLib_init(JNIEnv * env, jobject obj) {
    //__android_log_print(ANDROID_LOG_INFO, "hello-jni", "Jni::init");
    return setupGraphics() ? 1 : 0;
}

JNIEXPORT void JNICALL Java_com_alongthecloud_app_textclock_JniLib_changed(JNIEnv * env, jobject obj, jint width, jint height) {
    __android_log_print(ANDROID_LOG_INFO, "hello-jni", "Jni::changed %d %d", width, height);
    updateGraphics(width, height);
}

JNIEXPORT void JNICALL Java_com_alongthecloud_app_textclock_JniLib_update(JNIEnv * env, jobject obj, jlong deltaTime) {
    // __android_log_print(ANDROID_LOG_INFO, "hello-jni", "Jni::step");
    renderFrame(deltaTime / 1000.0f);
}

JNIEXPORT void JNICALL Java_com_alongthecloud_app_textclock_JniLib_setupAssetManager(JNIEnv *env,
                                                                                     jobject obj,
                                                                                     jobject assetManager)
{
    dry::Log(dry::LogInfo, "jni::setAssetManager");

    AAssetManager *mgr = AAssetManager_fromJava(env, assetManager);
    OGL::FileManager::getInstance()->setAssetManager((void *) mgr);
}


JNIEXPORT void JNICALL Java_com_alongthecloud_app_textclock_JniLib_message(JNIEnv *env,
                                                                                jobject obj,
                                                                                jstring name,
                                                                                jstring param0,
                                                                                jint param1,
                                                                                jint param2)
{
    if (app)
    {
        const char *szName = (*env).GetStringUTFChars(name, 0);
        const char *szParam0 = (*env).GetStringUTFChars(param0, 0);

        //__android_log_print(ANDROID_LOG_INFO, "hello-jni", "jni::message %s , %s, %d, %d", szName, szParam0, param1, param2);
        if (szName)
        {
            app->sendMessage(szName, szParam0, param1, param2);
        }
    }
}