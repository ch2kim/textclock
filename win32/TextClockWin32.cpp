// OGLTest.cpp : Defines the entry point for the application.
//

#include "glapplication.h"
#include <windows.h>
#include <shellapi.h>

// define the screen resolution ( 16 : 9 비율 )
#define SCREEN_WIDTH 540
#define SCREEN_HEIGHT 960

//#define SCREEN_WIDTH 960
//#define SCREEN_HEIGHT 540

BOOL _keys[256];
BOOL _mouses[1];

POINT _mousePoint[1];
POINT _mouseMove[1];

DWORD _time;

static void checkGlError()
{
	for (GLint error = glGetError(); error; error
		= glGetError())
	{
		char buffer[512];
		sprintf(buffer, "glError (0x%x)\n", error);
		OutputDebugStringA(buffer);
	}
}

const WCHAR* APPNAME = L"TextCloclApp";
const WCHAR* CLASSNAME = L"GLESApp";

GLApplication* app = NULL;

// the WindowProc function prototype
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// the entry point for any Windows program
int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	HWND hWnd;
	WNDCLASSEX wc;

	ZeroMemory(&wc, sizeof(WNDCLASSEX));

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WindowProc;
	wc.hInstance = hInstance;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.lpszClassName = CLASSNAME;

	RegisterClassEx(&wc);

	hWnd = CreateWindowEx(NULL, CLASSNAME, APPNAME,
		WS_OVERLAPPEDWINDOW, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT,
		NULL, NULL, hInstance, NULL);

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	RECT rt;
	GetClientRect(hWnd, &rt);

	dry::Renderer* renderer = new dry::Renderer(static_cast<void*>(hWnd), true, true);
	renderer->Init();

	app = new GLApplication();
	app->init(renderer);

	int width = rt.right - rt.left;
	int height = rt.bottom - rt.top;

	app->resize(width, height);

	app->sendMessage("loadfont", "nanumgothicb.fnt");
	app->sendMessage("text", "Mon. 1. 1", 1);
	app->sendMessage("text", "AM  9 : 00", 2);

	DWORD baseTime = GetTickCount();

	DragAcceptFiles(hWnd, TRUE);

	MSG msg;
	do
	{
		while (TRUE)
		{
			while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			if (msg.message == WM_QUIT)
				break;

			DWORD currentTime;
			DWORD deltaTime;

			currentTime = GetTickCount();
			deltaTime = currentTime - baseTime;
			baseTime = currentTime;

			app->update(deltaTime / 1000.f);
			app->render();
		}

	} while (0);

	DISPOSE(app);
	DISPOSE(renderer);

	return msg.wParam;
}

// this is the main message handler for the program
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int num = 0;

	switch (message)
	{
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return 0;
	} break;

	case WM_SIZE:
		if (app)
		{
			RECT rt;
			GetClientRect(hWnd, &rt);

			int width = rt.right - rt.left;
			int height = rt.bottom - rt.top;

			app->resize(width, height);
		}
		return 0;

	case WM_KEYDOWN:
		_keys[wParam] = TRUE;
		return 0;
	case WM_KEYUP:
		_keys[wParam] = FALSE;

		// 키보드 입력을 통한 테스트 
		if (wParam == VK_DOWN)
		{
			static int shadowOffset = 0;
			shadowOffset += 5;

			app->sendMessage("shadow", NULL, shadowOffset, shadowOffset);
		}

		return 0;

	case WM_LBUTTONDOWN:
		_mouses[0] = TRUE;

		_mousePoint[0].x = LOWORD(lParam);
		_mousePoint[0].y = HIWORD(lParam);

		_mouseMove[0].x = 0;
		_mouseMove[0].y = 0;

		return 0;
	case WM_LBUTTONUP:
		_mouses[0] = FALSE;

		_mouseMove[0].x = LOWORD(lParam) - _mousePoint[0].x;
		_mouseMove[0].y = HIWORD(lParam) - _mousePoint[0].y;

		_mousePoint[0].x = LOWORD(lParam);
		_mousePoint[0].y = HIWORD(lParam);

		return 0;

	case WM_MOUSEMOVE:
		_mouseMove[0].x = LOWORD(lParam) - _mousePoint[0].x;
		_mouseMove[0].y = HIWORD(lParam) - _mousePoint[0].y;

		_mousePoint[0].x = LOWORD(lParam);
		_mousePoint[0].y = HIWORD(lParam);
		return 0;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}
