package com.alongthecloud.app.textclock;

import android.content.Context;
import android.content.SharedPreferences;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import java.lang.reflect.Method;
import java.util.Calendar;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Na on 2017-09-16.
 */

public class MyRenderer implements GLSurfaceView.Renderer {
    public static Context baseContext = null;
    public static Handler mainHandler = null;

    private static final String TAG = "MyRenderer";

    private long baseTime;
    private long updateTime;

    private int alaramMode = 0;
    private int langType = 0;

    private int checkedPerMin =-1;

    private boolean isInitailized = false;

    public MyRenderer() {
        baseTime = System.currentTimeMillis();
    }

    @Override
    public synchronized void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        isInitailized = JniLib.init() != 0 ? true : false;

        if (isInitailized) {
            baseTime = System.currentTimeMillis();
            updateTime = baseTime;

            setupDate(true);
        }
    }

    @Override
    public synchronized void onSurfaceChanged(GL10 gl10, int width, int height) {
        if (isInitailized) {
            JniLib.changed(width, height);
            setupDate(true);
        }
    }

    @Override
    public synchronized void onDrawFrame(GL10 gl10) {
        long currentTime = System.currentTimeMillis();

        if (isInitailized) {
            long deltaTime = currentTime - baseTime;
            baseTime = currentTime;

            setupDate(false);
            JniLib.update(deltaTime);
        }
    }

    private synchronized void setupDate(boolean forceUpdate) {
        long currentTime = System.currentTimeMillis();

        if (!forceUpdate) {
            if (currentTime - updateTime < 1 * 1000)
                return;
        }

        // Log.d(TAG, "setupDate");

        int alramInterval = 0;
        if (baseContext != null) {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(baseContext);
            String fontname = sharedPref.getString("fonttype", "nanumgothicb.fnt");

            int textcolor1 = sharedPref.getInt("textcolor1", 0xffffffff);
            int textcolor2 = sharedPref.getInt("textcolor2", 0xffffffff);

            int bgcolor = sharedPref.getInt("bgcolor", 0x00000000);
            boolean shadow = sharedPref.getBoolean("shadow", false);
            String shadowOffset = sharedPref.getString("shadowoffset", "1");
            String mode = sharedPref.getString("mode", "0");

            JniLib.message("loadfont", fontname, 0, 0);
            JniLib.message("textcolor", "", textcolor1, textcolor2);
            JniLib.message("bgcolor", "", bgcolor, 0);

            try {
                int m = Integer.parseInt(mode);
                alaramMode = m % 10;
                langType = m / 10;

                JniLib.message("mode", "", m, 0);

                int n = Integer.parseInt(shadowOffset);
                JniLib.message("shadow", "", shadow ? n : 0, 0);

                alramInterval = Integer.parseInt(sharedPref.getString("alraminterval", "0"));
            }
            catch (Exception e)
            {
            }
        }

        updateTime = currentTime;

        CalendarText ct = new CalendarText(langType != 0);

        JniLib.message("text", ct.text1, 1, 0);
        JniLib.message("text", ct.text2, 2, 0);

        if (mainHandler == null)
            return;

        int min = ct.min;

        if (checkedPerMin == min)
            return;

        if (alramInterval == 0)
            return;

        boolean playAlram = false;

        if (min == 00 && alramInterval >= 1) playAlram = true;
        if (min == 30 && alramInterval >= 2) playAlram = true;
        if (alramInterval == 3) playAlram = true;

        if (playAlram) {
            checkedPerMin = min;

            try {
                switch (alaramMode) {
                    case 1:
                        mainHandler.sendEmptyMessage(1);
                        break;
                    case 2:
                        mainHandler.sendEmptyMessage(2);
                        break;
                }
            }
            catch (Exception e) {

            }
        }
    }
}
