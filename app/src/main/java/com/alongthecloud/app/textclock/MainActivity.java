package com.alongthecloud.app.textclock;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends Activity {

    private Handler mainHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    MyPlaySound.PlayRingtone(getApplicationContext());
                    break;
                case 2:
                    MyPlaySound.PlayTTS(getApplicationContext());
                    break;
            }
        }
    };

    private GLSurfaceView GLView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // setTheme(android.R.style.Theme_NoTitleBar_Fullscreen);

        MyRenderer.baseContext = getBaseContext();
        MyRenderer.mainHandler = mainHandler;

        JniLib.setupAssetManager(getBaseContext().getAssets());

        if (hasGLES20()) {
            GLView = new GLSurfaceView(this);
            GLView.setEGLContextClientVersion(2);
            GLView.setPreserveEGLContextOnPause(true);

            MyRenderer renderer = new MyRenderer();
            GLView.setRenderer(renderer);

            setContentView(GLView);
        }

        final Context c = this;

        GLView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                startActivity(new Intent(c, TextClockSettings.class));
                return true;
            }
        });

        String message = getString(R.string.ts_message);
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private boolean hasGLES20() {
        ActivityManager am = (ActivityManager)
                getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo info = am.getDeviceConfigurationInfo();

        return info.reqGlEsVersion >= 0x20000;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (GLView != null) {
            GLView.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (GLView != null) {
            GLView.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (GLView != null) {
            GLView = null;
        }

        JniLib.destroy();

        if (MyPlaySound.tts != null) {
            MyPlaySound.tts.stop();
            MyPlaySound.tts.shutdown();
            MyPlaySound.tts = null;
        }
    }
}
