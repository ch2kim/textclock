#pragma once

#ifndef NODE2D_HPP
#define NODE2D_HPP

#include "Node.hpp"
#include <system/MathStruct.h>

namespace OGL
{
	class RenderNode2D : public RenderNode
	{
	public:
		RenderNode2D() 
		{
		
		}

		virtual ~RenderNode2D()
		{

		}

	//	const Rect<float>& getRegion() { return _region; }
	//	void setRegion(Rect<float>& region) { _region = region; onRegionChanged(); }

	//protected:
	//	Rect<float> _region;

	//	virtual void onRegionChanged() = 0;
	};
}

#endif