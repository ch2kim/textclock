//
//  Texture.h
//  dryGL
//
//  Created by Jordi Ros on 15/02/13.
//  Copyright (c) 2013 Jordi Ros. All rights reserved.
//

#ifndef DRY_GRAPHICS_TEXTURE_H_
#define DRY_GRAPHICS_TEXTURE_H_

namespace dry {

class Texture
{
public:
    struct Params
    {
        Params()
        {
            Bilinear = true;
			Repeat = false;
            Mipmaps  = false;
        }

        bool    Bilinear;
		bool	Repeat;
        bool    Mipmaps;
    };
public:
                Texture         () { m_Handle = 0; }
               ~Texture         () { Free(); }

    bool        InitWithData    (int width, int height, PixelFormat format, Params const &params, void const *data);
	bool		InitWithImage	(Image& image, Params const &params);
    bool        InitWithHandle  (int width, int height, PixelFormat format, int handle);
	bool		InitWithAsset	(const char* filename, Params const &params);

    void        Free            ();

    void        Update          (void const *data);

    void        Bind            (int stage);
    void        Unbind          ();
    
	int			GetWidth()		{ return m_Width; }
	int			GetHeight()		{ return m_Height;  }
private:
    
    int         GetGLFormat     ();
    int         GetGLType       ();
    
private:
    
    int         m_Handle;
    int         m_Width;
    int         m_Height;
    PixelFormat m_Format;
    Params      m_Params;
    int         m_Target;
    bool        m_Release;
};

}

#endif
