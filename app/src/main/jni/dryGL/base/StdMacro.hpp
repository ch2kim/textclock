#pragma once

#ifndef _STDMACRO_HPP
#define _STDMACRO_HPP

#include <stdlib.h>
#include <memory.h>
#include <assert.h>

namespace OGL
{
	inline void SAFE_MEMCPY(void* dst, const void* src, size_t size)
	{
		if (dst != 0)
			memcpy(dst, src, size);
	}
};

//template< class T > void SAFE_DELETE(T*& pVal)
//{
//	delete pVal;
//	pVal = NULL;
//}
//
//template< class T > void SAFE_DELETE_ARRAY(T*& pVal)
//{
//	delete[] pVal;
//	pVal = NULL;
//}

#ifdef _DEBUG
#ifndef _ASSERT
	#define _ASSERT(n)	assert(n)
#endif

#ifndef _VERIFY
	#define _VERIFY(n)	assert(n)
#endif

#else
#ifndef _ASSERT
#define _ASSERT(n)	n
#endif

#ifndef _VERIFY
	#define _VERIFY(n)	n
#endif
#endif

#ifndef DISALLOW_COPY_AND_ASSIGN
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
  TypeName(const TypeName&);               \
  void operator=(const TypeName&)

#endif

#endif