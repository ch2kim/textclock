#include <stdio.h>

#ifndef WIN32

#include "android_fopen.h"
#include <errno.h>
#include <android/asset_manager.h>

int android_read(void *cookie, char *buf, int size)
{
    return AAsset_read((AAsset *) cookie, buf, size);
}

int android_write(void *cookie, const char *buf, int size)
{
    return EACCES; // can't provide write access to the apk
}

fpos_t android_seek(void *cookie, fpos_t offset, int whence)
{
    return AAsset_seek((AAsset *) cookie, offset, whence);
}

long android_size(void* cookie)
{
    return AAsset_getLength((AAsset *)cookie);
}

void android_close(void *cookie)
{
    AAsset_close((AAsset *) cookie);
}

void* android_open(AAssetManager *manager, const char* fname)
{
    AAsset *asset = AAssetManager_open(manager, fname, 0);
    return (void*)asset;
}

#endif