package com.alongthecloud.app.textclock;

import java.util.Calendar;

/**
 * Created by Na on 2017-10-08.
 */

public class CalendarText {

    public int am_pm;
    public int day;
    public int month;
    public int week;

    public int hour;
    public int min;

    public String text1;
    public String text2;

    public CalendarText(boolean hangul) {
        Calendar cal = Calendar.getInstance();

        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH) + 1;
        week = cal.get(Calendar.DAY_OF_WEEK);

        hour = cal.get(Calendar.HOUR_OF_DAY);
        min = cal.get(Calendar.MINUTE);

        if (hour == 0) {
            am_pm =  0; //"AM";
            hour = 12;
        } else if (hour < 12) {
            am_pm = 0; //"AM";
        } else if (hour == 12) {
            am_pm = 1; //"PM";
        } else {
            am_pm = 1; //"PM";
            hour -= 12;
        }

        if (hangul) { // 한글 모드
            String weekText = "  .";

            switch(week)
            {
                case 1: weekText = "일."; break;
                case 2: weekText = "월."; break;
                case 3: weekText = "화."; break;
                case 4: weekText = "수."; break;
                case 5: weekText = "목."; break;
                case 6: weekText = "금."; break;
                case 7: weekText = "토."; break;
            }

            text1 = String.format("%s %2d월 %2d일", weekText, month, day);

            String hourTypeTxt[] = { "한", "두", "세", "네", "다섯", "여섯", "일곱", "여덟", "아홉", "열", "열한", "열두" };
            String minTypeTxt[] = { "일", "이", "삼", "사", "오", "육", "칠", "팔", "구", "십" };
            String ampmTypeTxt[] = { "오전", "오후" };

            text2 = ampmTypeTxt[am_pm];
            text2 += " ";
            text2 += hourTypeTxt[hour - 1];
            text2 += "시 ";

            int high_m = min / 10;
            int low_m  = min % 10;

            if (high_m != 0)
            {
                if (high_m != 1) {
                    text2 += minTypeTxt[high_m-1];
                }

                text2 += "십";
            }

            if (low_m == 0 && high_m == 0)
            {
                text2 += "정각";
            }
            else
            {
                if (low_m != 0)
                {
                    text2 += minTypeTxt[low_m-1];
                }
                text2 += "분";
            }
        }
        // 숫자 모드
        else {
            String weekText = "   .";

            switch(week)
            {
                case 1: weekText = "Sun."; break;
                case 2: weekText = "Mon."; break;
                case 3: weekText = "Tue."; break;
                case 4: weekText = "Wed."; break;
                case 5: weekText = "Thu."; break;
                case 6: weekText = "Fri."; break;
                case 7: weekText = "Sat."; break;
            }

            String ampmTypeTxt[] = { "AM", "PM" };

            text1 = String.format("%s %2d.%2d", weekText, month, day);
            text2 = String.format("%s %2d : %02d", ampmTypeTxt[am_pm], hour, min);
        }
    }
}
