//
//  Renderer.h
//  dryGL
//
//  Created by Jordi Ros on 15/02/13.
//  Copyright (c) 2013 Jordi Ros. All rights reserved.
//

#ifndef DRY_GRAPHICS_RENDERER_H_
#define DRY_GRAPHICS_RENDERER_H_

#ifdef WIN32
	#include <Windows.h>
#endif

namespace dry {
    
class Renderer
{
public:
                        Renderer();
                        Renderer        (void* handle, bool depth, bool stencil);
                       ~Renderer        ();
    void				Init();

    void                Begin           ();
    void                End             ();

    void                Clear           (bool color = true, bool depth = true, bool stencil = true);
    
    void                DrawArrays      (int mode, int count);
    void                DrawElements    (int mode, int count, int type);
    
    void                SetViewport     (int x, int y, int w, int h);
    void                SetClearColor   (glm::vec4 const &color, float depth, int stencil);
	void				SetBlend		(bool enable);
    void                SetBlendMode    (BlendMode blend);

    glm::ivec4 const   &GetViewport     () { return m_Viewport; }
    //uint                GetColorBuffer  () { return m_ColorRenderBuffer; }
    //uint                GetDepthBuffer  () { return m_DepthRenderBuffer; }
    //uint                GetFrameBuffer  () { return m_FrameBuffer; }
    bool                GetRendering    () { return m_Rendering; }

	void				Present();

private:
    
    glm::ivec4          m_Viewport;
    glm::vec4           m_ClearColor;
    float               m_ClearDepth;
    int		            m_ClearStencil;
    BlendMode           m_BlendMode;
	bool				m_Blend;
    bool                m_Rendering;

	bool				m_owned;


#ifdef WIN32
	void*				eglDisplay;
	void*				eglContext;
	void*				eglSurface;

	HWND				m_hWnd;
	HDC					m_hDC;

	bool				InitContext(HWND hWnd, bool depth, bool stencil);
	void				FreeContext();
#endif

	//uint                m_ColorRenderBuffer;
	//uint                m_DepthRenderBuffer;
	//uint                m_FrameBuffer;
};

}

#endif
