#ifndef OGL_MATHSTRUCT_H
#define OGL_MATHSTRUCT_H

namespace OGL
{
	template<typename T>
	struct Rect
	{
		T left;
		T top;
		T right;
		T bottom;

        Rect()
        {
        }

		Rect(T l, T t, T r, T b)
		{
			left = l;
			top = t;
			right = r;
			bottom = b;
		}

		T getWidth()
		{
			return right - left;
		}

		T getHeight()
		{
			return bottom - top;
		}

		bool operator==(const Rect<T>& other)
		{
			return (left == other.left
				&& top == other.top
				&& right == other.right
				&& bottom == other.bottom);
		}

		bool operator!=(const Rect<T>& other)
		{
			return (left != other.left
				|| top != other.top
				|| right != other.right
				|| bottom != other.bottom);
		}
	};
};

#endif