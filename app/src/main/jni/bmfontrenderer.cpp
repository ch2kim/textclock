#include "bmfontrenderer.h"

#include <system/FileManager.h>
#include <system/DataBuffer.h>
#include <engine/ShaderManager.hpp>

namespace OGL
{

	BMFontRender::BMFontRender()
	{
		_glProgram = nullptr;
		_mvpMatrix = nullptr;
		_ucolor = nullptr;
		_uvtranslate = nullptr;

		_bmsize = 0;
		_bmfont = nullptr;

		_align = AlignType::Left;
		_verticalAlign = VerticalAlignType::Center;

		_fontColor = glm::vec4(1, 1, 1, 1);
		_shadowOffset = 0;

		_needtoUpdate = true;
	}

	BMFontRender::~BMFontRender()
	{
		destroy();
	}

	void BMFontRender::destroy()
	{
		_bmfont = nullptr;
		_meshList.clear();
	}

	void BMFontRender::initShader()
	{
		_ASSERT(_glProgram == nullptr);

		_glProgram = OGL::ShaderManager::getInstance().getProgram(OGL::ShaderFlag::BMFONT_SHADER);
		dry::Uniform* texture0 = _glProgram->GetUniformByName("CC_Texture0");
		if (texture0)
			texture0->Update(0);

		_mvpMatrix = _glProgram->GetUniformByName("CC_MVPMatrix");
		_ucolor = _glProgram->GetUniformByName("u_color");
		_uvtranslate = _glProgram->GetUniformByName("u_uvtranslate");
		if (_uvtranslate)
			_uvtranslate->Update(glm::vec2(0, 0));
	}

	bool BMFontRender::init()
	{
		//FileManager* fm = FileManager::getInstance();	
		initShader();
		return true;
	}

	void BMFontRender::setBMFont(BMFont* bmfont)
	{
		_bmfont = bmfont;
		if (_bmfont)
		{
			_meshList.resize(_bmfont->getPageCount());
			_needtoUpdate = !clearUpdateBuffer();
		}
	}

	void BMFontRender::setFontSize(int size)
	{
		if (_bmsize != size)
		{
			_bmsize = size;
			_needtoUpdate = true;
		}
	}

	void BMFontRender::setText(const wchar_t* u16text)
	{
		if (wcscmp(_u16text.c_str(), u16text) != 0)
		{
			_u16text = u16text;
			_needtoUpdate = true;
		}
	}

	void BMFontRender::setColor(const glm::vec4& color)
	{
		_fontColor = color;
	}

	void BMFontRender::setAlign(AlignType align)
	{
		if (_align != align)
		{
			_align = align;
			_needtoUpdate = true;
		}
	}

	void BMFontRender::setVertialAlign(VerticalAlignType align)
	{
		if (_verticalAlign != align)
		{
			_verticalAlign = align;
			_needtoUpdate = true;
		}
	}

	void BMFontRender::setShadowOffset(int offset)
	{
		_shadowOffset = offset;
	}

	Rect<int> BMFontRender::getBound()
	{
		_ASSERT(_bmfont != nullptr);

		int height = _bmfont->getHeight();

		int basex = 0;
		int basey = 0;

		Rect<int> bound;
		bound.left = 0;
		bound.top = 0;
		bound.right = 0;
		bound.bottom = 0;

		for (auto iter = _u16text.begin(); iter != _u16text.end(); ++iter)
		{
			int id = (int)(*iter);
			if (id == (int)L'\n')
			{
				basex = 0;
				basey += height;

				continue;
			}

			const BMFont::BMChar* bmchar = _bmfont->findFont(id);
			if (bmchar == nullptr)
				continue;

			basex += (bmchar->advance + bmchar->offsetX);
			if (bound.right < basex)
				bound.right = basex;
		}

		if (!_u16text.empty())
		{
			bound.bottom = basey + height;
		}

		return bound;
	}

	bool BMFontRender::clearUpdateBuffer()
	{
		if (!_bmfont)
			return false;

		// clear buffer
		for (size_t i = 0; i < _meshList.size(); ++i)
			_meshList[i].clear();

		// update buffer
		int basex = 0;
		int basey = 0;

		dry::Color fontColor(1.0f, 1.0f, 1.0f, 1.0f);

		int offsetX = 0;
		int offsetY = 0;

		const int fontsize = _bmfont->getSize();
		const int height = _bmfont->getHeight();
		const float scaleW = _bmfont->getScaleW();
		const float scaleH = _bmfont->getScaleH();

		float scale = (float)_bmsize / (float)fontsize;

		Rect<int> bound = getBound();
		bound.right = (int)(bound.right*scale);
		bound.bottom = (int)(bound.bottom*scale);

		switch (_align)
		{
		case AlignType::Left:
			break;
		case AlignType::Right:
			offsetX += -bound.getWidth();
			break;
		case AlignType::Center:
			offsetX += -bound.getWidth() / 2;
			break;
		}

		switch (_verticalAlign)
		{
		case VerticalAlignType::Top:
			offsetY = 0;
			break;
		case VerticalAlignType::Bottom:
			offsetY = -bound.getHeight();
			break;
		case VerticalAlignType::Center:
			offsetY = -bound.getHeight() / 2;
			break;
		}

		for (auto iter = _u16text.begin(); iter != _u16text.end(); ++iter)
		{
			int id = (int)(*iter);
			if (id == static_cast<int>(L'\n'))
			{
				basex = 0;
				basey += (int)(height * scale);

				continue;
			}

			const BMFont::BMChar* bmchar = _bmfont->findFont(id);
			if (bmchar == nullptr)
				continue;

			MeshFmt* mesh = &_meshList[bmchar->page];

			float u = bmchar->x / scaleW;
			float v = bmchar->y / scaleH;
			float s = (bmchar->x + bmchar->w) / scaleW;
			float t = (bmchar->y + bmchar->h) / scaleH;

			basex += (int)(bmchar->offsetX*scale);
			build(mesh, (float)(offsetX + basex), (float)(offsetY + basey + (bmchar->offsetY*scale)), (float)(bmchar->w *scale), (float)(bmchar->h *scale),
				u, v, s, t, fontColor, false);

			basex += (int)((bmchar->advance * scale));
		}

		return true;
	}

	void BMFontRender::build(MeshFmt* mesh, float x, float y, float width, float height, float u, float v, float s, float t, const dry::Color& color, bool flip)
	{
		auto& vertices = mesh->_vertexArray;
		auto& indices = mesh->_indexArray;

		float z = x + width;
		float w = y + height;

		if (flip)
		{
			y = height - y;
			w = height - w;
		}

		VertexFmt vt[4];
		vt[0].position = glm::vec3(x, y, 0.0f);
		vt[0].texCoord = glm::vec2(u, v);
		vt[0].color = color;

		vt[1].position = glm::vec3(z, y, 0.0f);
		vt[1].texCoord = glm::vec2(s, v);
		vt[1].color = color;

		vt[2].position = glm::vec3(x, w, 0.0f);
		vt[2].texCoord = glm::vec2(u, t);
		vt[2].color = color;

		vt[3].position = glm::vec3(z, w, 0.0f);
		vt[3].texCoord = glm::vec2(s, t);
		vt[3].color = color;

		int baseIndex = (int)vertices.size();

		vertices.push_back(vt[0]);
		vertices.push_back(vt[1]);
		vertices.push_back(vt[2]);
		vertices.push_back(vt[3]);

		int q0, q1, q2, q3;

		if (flip)
		{
			q0 = baseIndex;
			q1 = q0 + 2;
			q2 = q0 + 1;
			q3 = q0 + 3;
		}
		else
		{
			q0 = baseIndex;
			q1 = q0 + 1;
			q2 = q0 + 2;
			q3 = q0 + 3;
		}

		indices.push_back(q0);
		indices.push_back(q1);
		indices.push_back(q2);

		indices.push_back(q2);
		indices.push_back(q1);
		indices.push_back(q3);
	}

	void BMFontRender::update(float dt)
	{
		if (_needtoUpdate)
		{
			_needtoUpdate = !clearUpdateBuffer();
		}
	}

	void BMFontRender::draw(dry::Renderer* renderer, const glm::mat4& mvpMatrix)
	{
		if (_bmfont == nullptr)
			return;

		if (_meshList.empty())
			return;

		_ASSERT(_glProgram);
		_ASSERT(_mvpMatrix);

		renderer->SetBlend(true);
		renderer->SetBlendMode(dry::BlendAlpha);
		
		if (_shadowOffset != 0)
		{
			glm::vec4 black(0.0f, 0.0f, 0.0f, 0.5f);
			float offsetXY = (float)_shadowOffset;

			glm::mat4 shadowTransform = glm::translate(offsetXY, offsetXY, 0.0f) * getLocalTransform();
			drawFont(mvpMatrix*shadowTransform, black);
		}

		drawFont(mvpMatrix*getLocalTransform(), _fontColor);

		renderer->SetBlend(false);
	}

	void BMFontRender::drawFont(const glm::mat4& mat, const glm::vec4& color)
	{
		_mvpMatrix->Update(mat);
		if (_ucolor)
			_ucolor->Update(color);

		_glProgram->Bind();
	
		GLuint positionAttr = (GLuint)_glProgram->GetAttribByName("a_position");
		GLuint colorAttr = (GLuint)_glProgram->GetAttribByName("a_color");
		GLuint texcoordAttr = (GLuint)_glProgram->GetAttribByName("a_texCoord");

		for (size_t i = 0; i < _meshList.size(); ++i)
		{
			MeshFmt& mesh = _meshList[i];

			auto& vertices = mesh._vertexArray;
			auto& indices = mesh._indexArray;

			if (!vertices.empty() && !indices.empty())
			{
				glVertexAttribPointer(positionAttr, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFmt), &vertices[0].position);
				glVertexAttribPointer(colorAttr, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexFmt), &vertices[0].color);
				glVertexAttribPointer(texcoordAttr, 2, GL_FLOAT, GL_TRUE, sizeof(VertexFmt), &vertices[0].texCoord);

				glEnableVertexAttribArray(positionAttr);
				glEnableVertexAttribArray(colorAttr);
				glEnableVertexAttribArray(texcoordAttr);

				dry::Texture* texture = _bmfont->getTexture(i);

				texture->Bind(0);
				mesh.draw();
				texture->Unbind();
			}
		}

		_glProgram->Unbind();
	}
};