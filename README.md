# README
## 안드로이드 한글 텍스트 시계 + TTS

### 개요

취미삼아 간단히 만들어 본 안드로이드 한글 텍스트 시계 소스를 업로드 합니다.

ndk 에서 opengl es를 사용하여 비트맵 폰트를 렌더링 하고, 옵션에 따라 android api의 tts(text to speech) 기능을 이용하여 한글로 시각을 읽어줍니다.

제가 익숙한 방법인 옛날 방법으로 ndk 빌드 세팅을 했습니다.

android studio로 프로젝트를 열고 나서 sdk 및 ndk 경로 설정이 필요합니다.

오래된 코드이기 때문에 최신 버전의 android studio 에서 실행이 원활하지 않을 수 있습니다.

### 추가된 점

visual studio 파일이 추가되어, 윈도우 상에서 기본 렌더링 기능을 테스트 해 볼 수 있도록 수정했으며, 윈도우에서 opengl es 로 렌더링 하기 위해 PowerVR 사의 sdk를 사용하였습니다.

해당 sdk 는 https://github.com/powervr-graphics/Native_SDK 에서 다운로드 받을 수 있습니다.

해당 sdk 를 받으신 후 

* here_is_powervr_opengles_header_files.txt 파일이 있는 곳에 include폴더의 헤더파일 EGL, GLES2, GLES3, KHR 디렉토리(폴더)를 복사해주세요.
* here_is_powervr_opengles_lib_files.txt 파일이 있는 곳에 windows 32bit 라이브러리 파일 libEGL.lib, libGLES_CM.lib, libGLESv2.lib, PVRScopeDeveloper.lib 및 DLL 파일을 복사해주세요.
* 현재 x86 세팅으로만 빌드가 됩니다.

visual studio 2019 community 로 빌드 테스트를 했습니다.

### 사용된 라이브러리

* PowerVR SDK
* https://github.com/JordiRos/dryGL MIT License
* stb_image - Public domain
* https://github.com/attenzione/android-ColorPickerPreference Apache License 2.0 

### 사용된 도구

* http://www.angelcode.com/products/bmfont

### 개발 환경
 windows 10
 visual studio community 2015 update 3
 android studio 2.3.2

## License
```
MIT License
Copyright (c) 2017 CH Kim
```
