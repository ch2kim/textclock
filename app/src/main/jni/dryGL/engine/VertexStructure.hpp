#pragma once

#include "platform.h"

namespace OGL
{
#pragma pack(1)
	// Vertex Structure
	struct V3F_C4B
	{
		glm::vec3   position;
		dry::Color  color;
	};

	struct V3F_TEX
	{
		glm::vec3	position;
		glm::vec2	texCoord;
	};
	
	struct V3F_C4B_NRM
	{
		glm::vec3	position;
		dry::Color	color;
		glm::vec3	normal;
	};

	struct V3F_C4B_TEX
	{
		glm::vec3	position;
        dry::Color	color;
		glm::vec2	texCoord;
	};
#pragma pack()
};