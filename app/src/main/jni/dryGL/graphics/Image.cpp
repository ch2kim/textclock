//
//  Image.cpp
//  dryGL
//
//  Created by Jordi Ros on 15/02/13.
//  Copyright (c) 2013 Jordi Ros. All rights reserved.
//

#include "dry.h"
#include "Image.h"

#include "system/FileManager.h"
#include "stb/stb_image.h"

#include "externals/exif.h"

using namespace dry;


//------------------------------------------------------------------------------------------------
// InitWithData
//
//------------------------------------------------------------------------------------------------
bool Image::InitWithData(int width, int height, PixelFormat format, void const *data)
{
    m_Width  = width;
    m_Height = height;
    m_Format = format;
	void* pdata = malloc(width*height*GetBPP());
	m_Data = (uchar*)pdata;

	int size = width * height * GetBPP();

	if (data)
		memcpy(m_Data, data, size);

    return true;
}


bool Image::InitWithAsset(const char* filename)
{
	OGL::DataBuffer buffer;
	if (!OGL::FileManager::getInstance()->loadFromAsset(buffer, filename))
		return false;

	int width;
	int height;
	int channel;

	stbi_uc* data = stbi_load_from_memory((stbi_uc const*)buffer.getBufferPtr(), buffer.getBufferSize(), &width, &height, &channel, STBI_default);
	if (!data)
	{
		dry::Log(dry::LogError, "Image::InitWithAsset /", stbi_failure_reason());
		return false;
	}

	PixelFormat pf = PixelFormatUnknown;

	switch (channel)
	{
	case 1: pf = PixelFormatAlpha; break;
	case 3: pf = PixelFormatRgb24; break;
	case 4: pf = PixelFormatArgb32; break;
	}

	if (pf == PixelFormatUnknown)
	{
		stbi_image_free(data);
		data = NULL;
		return false;
	}

    m_Width  = width;
    m_Height = height;
    m_Format = pf;
	m_Data = (uchar*)data;
	
	return true;
}

bool Image::InitWithFile(const char* filename)
{
	FILE* fp = fopen(filename, "rb");
	if (!fp)
		return false;

	fseek(fp, 0L, SEEK_END);
	long fileSize = ftell(fp);
	fseek(fp, 0L, SEEK_SET);

	OGL::DataBuffer buffer;
	buffer.resize(fileSize);

	fread(buffer.getBufferPtr(), buffer.getBufferSize(), 1, fp);
	fclose(fp);

	const char* p = strrchr(filename, '.');

	char lowerExt[16];
	memset(lowerExt, 0, sizeof(lowerExt));

	if (p)
	{
		strcpy(lowerExt, p);
		for (int i = 0; i<16; ++i)
		{
			if (lowerExt[i] == 0)
				break;

			lowerExt[i] = (char)tolower(lowerExt[i]);
		}

		if (strcmp(lowerExt, ".jpg") == 0
			|| strcmp(lowerExt, ".jpeg") == 0)
		{
			easyexif::EXIFInfo result;
			int code = result.parseFrom((unsigned char*)buffer.getBufferPtr(), (unsigned long)buffer.getBufferSize());

			m_Orientation = (int)result.Orientation;
		}
	}

	int width;
	int height;
	int channel;

	stbi_uc* data = stbi_load_from_memory((stbi_uc const*)buffer.getBufferPtr(), buffer.getBufferSize(), &width, &height, &channel, STBI_default);
	if (!data)
	{
		dry::Log(dry::LogError, "Image::InitWithFile /", stbi_failure_reason());
		return false;
	}

	PixelFormat pf = PixelFormatUnknown;

	switch (channel)
	{
	case 1: pf = PixelFormatAlpha; break;
	case 3: pf = PixelFormatRgb24; break;
	case 4: pf = PixelFormatArgb32; break;
	}

	if (pf == PixelFormatUnknown)
	{
		stbi_image_free(data);
		data = NULL;
		return false;
	}

	m_Width = width;
	m_Height = height;
	m_Format = pf;
	m_Data = (uchar*)data;

	return true;
}

bool Image::InitWithImage(int width, int height, Image& image, int sx, int sy)
{
	if (!InitWithData(width, height, image.GetFormat(), NULL))
		return false;

	Clear(0x00);
	CopyFromImage(image, sx, sy);
	return true;
}

void Image::CopyFromImage(Image& image, int sx, int sy)
{
	int bpp = this->GetBPP();

	uchar* src = image.GetData();
	int srcPitch = image.GetWidth()*bpp;

	uchar* dest = GetData();
	int destPitch = GetWidth()*bpp;

	int begin_y = sy;
	int end_y = begin_y + GetHeight();

	do
	{
		if (begin_y >= image.GetHeight())
			break;
		else if (begin_y < 0)
			begin_y = 0;

		if (end_y < 0)
			break;
		else if (end_y > image.GetHeight())
			end_y = image.GetHeight();

		for (int y = begin_y; y < end_y; ++y)
		{
			if (y < 0 || y >= image.GetHeight())
				continue;

			int begin_x = sx;
			int end_x = begin_x + GetWidth();

			if (begin_x >= image.GetWidth())
				continue;
			else if (begin_x < 0)
				begin_x = 0;

			if (end_x < 0)
				continue;
			else if (end_x > image.GetWidth())
				end_x = image.GetWidth();

			for (int x = begin_x; x < end_x; ++x)
			{
				_ASSERT(x >= 0 && x < image.GetWidth());
				_ASSERT(y >= 0 && y < image.GetHeight());
				int si = x*bpp + y*srcPitch;

				int dx = x - sx;
				int dy = y - sy;
				_ASSERT(dx >= 0 && dx < GetWidth());
				_ASSERT(dy >= 0 && dy < GetHeight());
				int di = dx*bpp + dy*destPitch;

				uchar* psrc = src + si;
				uchar* pdest = dest + di;

				switch (bpp)
				{
				case 4: *pdest++ = *psrc++;
				case 3: *pdest++ = *psrc++;
				case 2: *pdest++ = *psrc++;
				case 1: *pdest++ = *psrc++;
				}
			} // x
		} // y
	} while (0);

	return;
}

void Image::Clear(unsigned char b)
{
	if (GetData())
	{
		int size = GetWidth() * GetHeight() * GetBPP();
		memset((void*)GetData(), b, size);
	}
}

void Image::GradientClear()
{
	if (GetData())
	{
		unsigned char* ptr = GetData();
		for (int y = 0; y < GetHeight(); ++y)
		{
			for (int x = 0; x < GetWidth(); ++x)
			{
				for (int n = 0; n < GetBPP(); ++n)
				{
					int index = (y*GetWidth() + x)*GetBPP();
					ptr[index] = (unsigned char)((y / (float)GetHeight()) * 255);
				}
			}
		}
	}
}

//------------------------------------------------------------------------------------------------
// Free
//
//------------------------------------------------------------------------------------------------
void Image::Free()
{
	SAFE_FREE(m_Data);
}


//------------------------------------------------------------------------------------------------
// GetBPP
//
//------------------------------------------------------------------------------------------------
int Image::GetBPP() const
{
    switch (m_Format)
    {
        case PixelFormatAlpha:   return 1;
        case PixelFormatRgb565:  return 2;
        case PixelFormatRgb24:   return 3;
        case PixelFormatArgb32:  return 4;
        default:                 return 0;
    }
}


//------------------------------------------------------------------------------------------------
// GetFormat
//
//------------------------------------------------------------------------------------------------
PixelFormat Image::GetFormat(int bpp) const
{
    switch (bpp)
    {
        case 1: return PixelFormatAlpha;
        case 2: return PixelFormatRgb565;
        case 3: return PixelFormatRgb24;
        case 4: return PixelFormatArgb32;
    }
    return PixelFormatUnknown;
}
