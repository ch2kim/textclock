//
//  Color.h
//  dryGL
//
//  Created by Jordi Ros on 15/02/13.
//  Copyright (c) 2013 Jordi Ros. All rights reserved.
//

#ifndef DRY_SYSTEM_COLOR_H_
#define DRY_SYSTEM_COLOR_H_

namespace dry {

// Byte Color
#pragma pack(1)
struct Color
{
    Color() { r = 0; g = 0; b = 0; a = 0; }

    Color(unsigned char _r, unsigned char _g, unsigned char _b, unsigned char _a) { Set(_r,_g,_b,_a); }
	Color(float _r, float _g, float _b, float _a) {	Set(_r, _g, _b, _a); }

    void Set(unsigned char _r, unsigned char _g, unsigned char _b, unsigned char _a)
    {
        r = _r;
        g = _g;
        b = _b;
        a = _a;
    }

	void Set(float _r, float _g, float _b, float _a)
	{
		r = (unsigned char)(_r*255);
		g = (unsigned char)(_g*255);
		b = (unsigned char)(_b*255);
		a = (unsigned char)(_a*255);
	}

    unsigned int Get() const
    {
        return a << 24 | r << 16 | g << 8 | b;
    }

    // Data
	unsigned char r;
    unsigned char g;
	unsigned char b;
    unsigned char a;
};
#pragma pack()

}

#endif
