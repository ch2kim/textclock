#ifndef _BMFONTRENDERER_H_
#define _BMFONTRENDERER_H_

#include "dry.h"
#include "bmfont.h"

#include <base/StdMacro.hpp>
#include <engine/Node2D.hpp>
#include <engine/VertexStructure.hpp>
#include <engine/glMesh.hpp>
#include <system/MathStruct.h>

#include <vector>
#include <map>
#include <string>

namespace OGL
{
	class BMFontRender : public RenderNode2D
	{
	public:
		enum class AlignType : unsigned char
		{
			Left,
			Right,
			Center
		};

		enum class VerticalAlignType : unsigned char
		{
			Top,
			Bottom,
			Center,
		};

		BMFontRender();
		virtual ~BMFontRender();

		bool init();
		void setBMFont(BMFont* bmfont);

		void setFontSize(int size);
		int getFontSize() { return _bmsize; }

		void setColor(const glm::vec4& color);
		void setText(const wchar_t* u16text);

		void setAlign(AlignType align);
		void setVertialAlign(VerticalAlignType align);
		void setShadowOffset(int offset);

		//void setRegion(const Rect<int>& region);

		Rect<int> getBound();

		virtual void update(float);
		virtual void draw(dry::Renderer*, const glm::mat4&);

	private:
		int _bmsize;
		BMFont* _bmfont;

		std::wstring _u16text;
		AlignType _align;
		VerticalAlignType _verticalAlign;
		int _shadowOffset;

		bool _needtoUpdate;

		typedef OGL::V3F_C4B_TEX VertexFmt;
		typedef OGL::GLMesh<VertexFmt> MeshFmt;

		dry::Shader* _glProgram;
		dry::Uniform* _mvpMatrix;
		dry::Uniform* _ucolor;
		dry::Uniform* _uvtranslate;

		glm::vec4 _fontColor;

		std::vector<MeshFmt> _meshList;

		void destroy();
		void initShader();

		bool clearUpdateBuffer();
		void build(MeshFmt* mesh, float x, float y, float width, float height, float u, float v, float s, float t, const dry::Color& color, bool flip);

		void drawFont(const glm::mat4& mat, const glm::vec4& color);
	};
};

#endif