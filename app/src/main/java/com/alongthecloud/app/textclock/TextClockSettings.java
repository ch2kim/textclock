package com.alongthecloud.app.textclock;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.app.Activity;
import android.preference.*;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.ads.*;

/**
 * Created by Na on 2017-09-03.
 */

public class TextClockSettings extends Activity {

    @Override
    protected void onCreate(Bundle saveInstanceState)
    {
        super.onCreate(saveInstanceState);

        getFragmentManager().beginTransaction().
                replace(android.R.id.content, new MySettingsFragment()).commit();

    }

    public static class MySettingsFragment extends PreferenceFragment {
        static final String TAG = "MySettingsFragment";

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View view = super.onCreateView(inflater, container, savedInstanceState);
            /**
             Add admob-smart banner
            Context c = container.getContext();
            MobileAds.initialize(c, "your app ID");

            AdView adView = new AdView(c);
            adView.setAdSize(AdSize.SMART_BANNER);
            adView.setAdUnitId("your Ad Unit ID");

            ((LinearLayout)view).addView(adView);

            AdRequest adRequest = new AdRequest.Builder()
                    //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .build();
            adView.loadAd(adRequest);
             */
            return view;
        }

        @Override
        public void onCreate(Bundle saveInstanceState) {
            super.onCreate(saveInstanceState);

            addPreferencesFromResource(R.xml.prefs);

            // 클릭 했을 때 처리
            setOnPreferenceClick(findPreference("aboutthisapp"));

            // 값이 바뀌었을 때 처리
            setOnPreferenceChange(findPreference("fonttype"));
            setOnPreferenceChange(findPreference("shadowoffset"));
            setOnPreferenceChange(findPreference("mode"));
            setOnPreferenceChange(findPreference("alraminterval"));

        }

        private void setOnPreferenceClick(Preference preference) {
            if (preference == null) {
                return;
            }

            preference.setOnPreferenceClickListener(onClickListener);
        }

        private Preference.OnPreferenceClickListener onClickListener = new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                String key = preference.getKey();

                if (key.equals("aboutthisapp")) {
                    Intent i = new Intent(getActivity(), CreditActivity.class);
                    startActivity(i);
                }

                return false;
            }
        };

        private void setOnPreferenceChange(Preference preference) {
            if (preference == null) {
                return;
            }

            preference.setOnPreferenceChangeListener(onPreferenceChangeListener);
            onPreferenceChangeListener.onPreferenceChange(
                    preference,
                    PreferenceManager.getDefaultSharedPreferences(
                            preference.getContext()).getString(
                            preference.getKey(), ""));
        }

        private Preference.OnPreferenceChangeListener onPreferenceChangeListener = new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (newValue != null) {
                    if (preference instanceof EditTextPreference) {
                        String stringValue = newValue.toString();
                        preference.setSummary(stringValue);

                    } else if (preference instanceof ListPreference) {
                        ListPreference listPreference = (ListPreference) preference;
                        String stringValue = newValue.toString();

                        int index = listPreference.findIndexOfValue(stringValue);
                        preference.setSummary(index >= 0 ? listPreference.getEntries()[index] : "null");
                    }
                }

                // preference.getEditor();
                return true;
            }
        };
    }
}
