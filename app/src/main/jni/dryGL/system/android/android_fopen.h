#ifndef ANDROID_FOPEN_H
#define ANDROID_FOPEN_H

#ifndef WIN32

#include <stdio.h>
#include <android/asset_manager.h>

#ifdef __cplusplus
extern "C" {
#endif

/* hijack fopen and route it through the android asset system so that
   we can pull things out of our packagesk APK */

void* android_open(AAssetManager *manager, const char* fname);

int android_read(void *cookie, char *buf, int size);
int android_write(void *cookie, const char *buf, int size);
fpos_t android_seek(void *cookie, fpos_t offset, int whence);
long android_size(void* cookie);
void android_close(void *cookie);

#ifdef __cplusplus
}
#endif

#endif

#endif