#pragma once

#ifndef NODE_HPP
#define NODE_HPP

#include "dry.h"
#include <vector>

namespace OGL
{
	class BaseNode
	{
	public:
		BaseNode()
			: _enable(true)
			, _order(0)
			, _userData(nullptr)
			, _parent(nullptr)
		{
		}

		virtual ~BaseNode() 
		{
		}

		virtual void update(float) = 0;
		virtual void draw(dry::Renderer* renderer, const glm::mat4&) = 0;

		void setEnable(bool enable) { _enable = enable; }
		bool getEnable() { return _enable; }

		void setOrder(int n) { _order = n; }
		int getOrder() const { return _order; }

		void setUserData(void* data) { _userData = data; }
		void* getUserData() { return _userData; }
	
		void attach(BaseNode* node);
		void dettach(BaseNode* node);

		void dettachFromParent();

		std::vector<BaseNode*>& getChildren() { return _children; }

	protected:
		bool _enable;
		int _order;

		BaseNode* _parent;
		std::vector<BaseNode*> _children;

		void* _userData;
	};

	class RenderNode : public BaseNode
	{
	public:
		RenderNode() 
		{
			_tscale = glm::vec3(1, 1, 1);
		}

		virtual ~RenderNode() 
		{
		}

		const glm::mat4& getLocalTransform() { return _localTransform; }
		void setLocalTransform(glm::mat4& mat) { _localTransform = mat; }

		void setPosition(const glm::vec3& position) { _tposition = position; }
		const glm::vec3& getPosition() { return _tposition; }

		void setScale(float s) { _tscale = glm::vec3(s, s, s); }
		const glm::vec3& getScale() { return _tscale; }

		void updateTransform();
	protected:
		glm::mat4 _localTransform;

		glm::vec3 _tscale;
		glm::quat _trotation;
		glm::vec3 _tposition;
	};
}

#endif