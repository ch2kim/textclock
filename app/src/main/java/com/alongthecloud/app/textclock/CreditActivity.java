package com.alongthecloud.app.textclock;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class CreditActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit);

        try {
            WebView wv = (WebView)findViewById(R.id.webView);
            wv.loadUrl("file:///android_asset/aboutthisapp.html");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
