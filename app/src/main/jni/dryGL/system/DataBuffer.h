#pragma once

#ifndef OGL_DATABUFFER_H
#define OGL_DATABUFFER_H

#include <stdlib.h>
#include <memory.h>

#include "base/StdMacro.hpp"

namespace OGL
{
	class DataBuffer
	{
	public:
		DataBuffer();
		DataBuffer(long size);

		virtual ~DataBuffer();

		void clear();
		void set(void* buf, long size);

		void resize(long size, bool clear = false);

		void* getBufferPtr();
		long getBufferSize();

	protected:
		void* _buffer;
		long _size;

	private:
		DISALLOW_COPY_AND_ASSIGN(DataBuffer);
	};

	inline void DataBuffer::clear()
	{
		if (_buffer)
		{
			free(_buffer);
			_buffer = NULL;
		}

		_size = 0;
	}

	inline void* DataBuffer::getBufferPtr()
	{
		return _buffer;
	}

	inline long DataBuffer::getBufferSize()
	{
		return _size;
	}
};

#endif