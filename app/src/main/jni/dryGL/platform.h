#pragma once

#ifndef PLATFORM_H
#define PLATFORM_H

#ifdef WIN32	// PowerVR2 OpenGL ES 2 Emulator
    #include <Win32/GLES2/gl2.h>
    #include <win32/GLES2/gl2ext.h>
    #include <Win32/EGL/egl.h>
#else
    #include <jni.h>
    #include <android/log.h>

    #include <GLES2/gl2.h>
    #include <GLES2/gl2ext.h>
#endif
#ifndef OOGL_PLATFORM_GLES
    #define OOGL_PLATFORM_GLES
#endif

#endif