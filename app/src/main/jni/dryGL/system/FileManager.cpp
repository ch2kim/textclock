#include "FileManager.h"

#include <base/StdMacro.hpp>
#include <system/DataBuffer.h>
#include <dry.h>

#ifndef WIN32
	#include "android/android_fopen.h"
	#include <android/asset_manager_jni.h>
	#include <android/log.h>
#endif

const char* WriteablePath = "data/";

namespace OGL
{
	FileManager* FileManager::getInstance()
	{
		static FileManager instance;
		return &instance;
	}

	FileManager::FileManager()
	{
	}

	FileManager::~FileManager()
	{

	}

	const char* FileManager::getWriteablePath()
	{
		return WriteablePath;
	}

    void FileManager::setAssetManager(void *assetManager)
    {
        _assetManager = assetManager;
    }

	bool FileManager::loadFromAsset(DataBuffer& buffer, const char* filename, bool binary /* = true */)
	{
		void* buf = NULL;
		long size = 0;

		if (!loadFromAsset(&buf, &size, filename, binary))
			return false;

		buffer.set(buf, size);
		return true;
	}

	bool FileManager::loadFromAsset(void** buffer, long* size, const char* filename, bool binary /* = true */)
	{
		if (buffer == NULL)
			return false;

	#ifdef WIN32
		char mode[3];
		mode[0] = 'r';
		mode[1] = binary ? 'b' : 't';
		mode[2] = '\0';

		FILE* fp = fopen(filename, mode);

		if (!fp)
		{
			dry::Log(dry::LogWarning, "faied file open : %s", filename);
			return false;
		}

		fseek(fp, 0L, SEEK_END);
		long fileSize = ftell(fp);
		fseek(fp, 0L, SEEK_SET);

		if (fileSize == 0)
		{
			fclose(fp);
			return false;
		}

		void* buf = malloc(fileSize);
		memset(buf, 0, fileSize);

		fread(buf, fileSize, 1, fp);
		fclose(fp);

		*buffer = buf;
		*size = fileSize;
	#else
		void* fp = android_open((AAssetManager*)_assetManager, filename);
		if (!fp)
			return false;

		long fileSize = android_size(fp);
		long bufferSize = fileSize;
		if (!binary)
			bufferSize += 16;

		void* buf = malloc(bufferSize);
		memset(buf, 0, bufferSize);

		android_read(fp, (char*)buf, fileSize);
		android_close(fp);

		*buffer = buf;
		*size = bufferSize;
	#endif
		return true;
	}

	void FileManager::saveToData(const char* filename, DataBuffer& buffer)
	{

	}
}
