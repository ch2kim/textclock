//
// Created by CHKim on 2016-02-03.
//

#include "GLApplication.h"
#include <base/StdMacro.hpp>
#include <engine/Node.hpp>
#include <engine/ShaderManager.hpp>
#include <system/FileManager.h>
#include <system/DataBuffer.h>

#include <algorithm>

#include "bmfontrenderer.h"

using namespace dry;

//////////////////////////////////////////////////////////////////////////
GLApplication::GLApplication()
	: _renderer(nullptr)
	, _time(0.0f)
	, _deltaTime(0.0f)
	, _backgroundColor(0, 0, 1)
	, _numericMode(false)
	, _landscape(false)

{
	OGL::ShaderManager::init();

	_bmfont = nullptr;
	_bmtext1 = nullptr;
	_bmtext2 = nullptr;
}

GLApplication::~GLApplication()
{
	destroy();

	OGL::ShaderManager::quit();
}

void GLApplication::destroy()
{
	clearScene();

	DISPOSE(_bmfont);
}

bool GLApplication::init(dry::Renderer* renderer)
{
	if (!renderer)
		return false;

	_renderer = renderer;

	//_cam.LookAt(glm::vec3(0, 0, -1), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	dry::Log(dry::LogInfo, "GL_VENDOR : %s", glGetString(GL_VENDOR));
	dry::Log(dry::LogInfo, "GL_RENDERER : %s", glGetString(GL_RENDERER));

	return true;
}

void GLApplication::initScene(bool landscape)
{
	OGL::BMFontRender* bmtext1 = new OGL::BMFontRender();
    bmtext1->init();

	_root.push_back(bmtext1);
	_bmtext1 = bmtext1;

	OGL::BMFontRender* bmtext2 = new OGL::BMFontRender();
    bmtext2->init();

	_root.push_back(bmtext2);
	_bmtext2 = bmtext2;

	initLayout(landscape);
}

void GLApplication::initLayout(bool landscape)
{
	if (!_bmtext1 || !_bmtext2)
		return;

	_bmtext1->setBMFont(_bmfont);
	_bmtext1->setText(L"   , 00.00");

	_bmtext2->setBMFont(_bmfont);
	_bmtext2->setText(L"AM 00 : 00");

	_bmtext1->setAlign(OGL::BMFontRender::AlignType::Center);
	_bmtext1->setVertialAlign(OGL::BMFontRender::VerticalAlignType::Center);
	_bmtext2->setAlign(OGL::BMFontRender::AlignType::Center);
	_bmtext2->setVertialAlign(OGL::BMFontRender::VerticalAlignType::Center);

	if (landscape)
	{

		_bmtext1->setPosition(glm::vec3(0, -124, 0));
		_bmtext1->updateTransform();
		_bmtext1->setFontSize(_numericMode ? 180 : 150);

		_bmtext2->setPosition(glm::vec3(0, 124, 0));
		_bmtext2->updateTransform();
		_bmtext2->setFontSize(_numericMode ? 240 : 160);
	}
	else
	{
		//
		_bmtext1->setPosition(glm::vec3(0, -124, 0));
		_bmtext1->updateTransform();
		_bmtext1->setFontSize(_numericMode ? 120 : 80);

		_bmtext2->setPosition(glm::vec3(0, 124, 0));
		_bmtext2->updateTransform();
		_bmtext2->setFontSize(_numericMode ? 150 : 90);
	}

	_landscape = landscape;
}

void GLApplication::clearScene()
{
	for (size_t i = 0; i < _root.size(); ++i)
	{
		auto node = _root[i];

		void* userData = node->getUserData();
		if (userData)
		{
			free(userData);
		}

		DISPOSE(node);
	}

	_root.clear();

	_bmtext1 = nullptr;
	_bmtext2 = nullptr;
}

void GLApplication::resize(int width, int height)
{
	if (_renderer)
	{
		_renderer->SetViewport(0, 0, width, height);

		const int halfOfWidth = 360;
		const int halfOfHeight = 640;

        bool landscape = width > height;

        if (landscape)
		{
            _cam.Init((float) -halfOfHeight, (float) halfOfHeight, (float) -halfOfWidth,
                      (float) halfOfWidth, 100.0f, -100.0f);
        }
		else
		{
			_cam.Init((float)-halfOfWidth, (float)halfOfWidth, (float)-halfOfHeight, (float)halfOfHeight, 100.0f, -100.0f);
		}

		clearScene();
		initScene(landscape);
	}
}

bool GLApplication::update(float dt)
{
	glm::ivec4 vp = _renderer->GetViewport();

	size_t sceneCount = _root.size();
	for (size_t i = 0; i < sceneCount; ++i)
	{
		OGL::RenderNode* node = _root[i];
		node->update(dt);
	}

	_time += dt;
	_deltaTime = dt;

	return true;
}

void GLApplication::render()
{
	auto renderer = _renderer;

	_renderer->SetClearColor(glm::vec4(_backgroundColor, 1.0f), 1.0f, 0);
	renderer->Clear();
	renderer->Begin();

	glm::mat4 viewproj = (_cam.GetProjection() * _cam.GetView());

	glDisable(GL_DEPTH_TEST);
	
	for (size_t i = 0; i<_root.size(); ++i)
	{
		OGL::BaseNode* node = _root[i];
		node->draw(renderer, viewproj);
	}

	glEnable(GL_DEPTH_TEST);

	renderer->End();
	renderer->Present();
}

glm::vec3 IntColorToVec3(int color)
{
	glm::vec3 c;
	// c.a = (color >> 24 & 0xff)/255.0f;
	c.r = (color >> 16 & 0xff)/255.0f;
	c.g = (color >> 8 & 0xff)/255.0f;
	c.b = (color & 0xff)/255.0f;

	return c;
}

void GLApplication::sendMessage(const char *msg, const char* param0, int param1, int param2)
{
    // dry::Log(dry::LogInfo, "hello-jni", "GLApplication::message %s , %s, %d, %d", msg, param0, param1, param2);
	if (strcmp(msg, "text") == 0)
	{
		wchar_t wparam[128];
		mbstowcs(wparam, param0, 128);

		switch(param1)
		{
			case 1:
				if (_bmtext1)
					_bmtext1->setText(wparam);
				break;
			case 2:
				if (_bmtext2)
					_bmtext2->setText(wparam);
				break;
			default:
				break;
		}
	}
	else if (strcmp(msg, "loadfont") == 0)
	{
		OGL::BMFont* bmfont = new OGL::BMFont();
		if (bmfont->initWithFile(param0))
		{
			DISPOSE(_bmfont);
			_bmfont = bmfont;
            if (_bmtext1)
			    _bmtext1->setBMFont(bmfont);

            if (_bmtext2)
			    _bmtext2->setBMFont(bmfont);
		}
		else
		{
			DISPOSE(bmfont);
		}
	}
	else if (strcmp(msg, "textcolor") == 0)
	{
		glm::vec4 textColor1 = glm::vec4(IntColorToVec3(param1), 1.0f);
		glm::vec4 textColor2 = glm::vec4(IntColorToVec3(param2), 1.0f);

		if (_bmtext1)
			_bmtext1->setColor(textColor1);

		if (_bmtext2)
			_bmtext2->setColor(textColor2);
	}
	else if (strcmp(msg, "bgcolor") == 0)
	{
		_backgroundColor = (IntColorToVec3(param1));
	}
	else if (strcmp(msg, "mode") == 0)
	{
		int language = param1 / 10;
        _numericMode = (language == 0);

		initLayout(_landscape);
	}
	else if (strcmp(msg, "shadow") == 0)
	{
		if (_bmtext1)
			_bmtext1->setShadowOffset(param1);

		if (_bmtext2)
			_bmtext2->setShadowOffset(param1);
	}
}