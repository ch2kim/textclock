#ifndef _SHADERMANAGER_H_
#define _SHADERMANAGER_H_

/*
Copyright (C) 2015 CH Kim
*/

#include "dry.h"
#include <map>
#include <string>

namespace OGL
{
	enum VertexFlag
	{
		VF_POSITION = 0x000001,
		VF_NORMAL = 0x000002,
		VF_COLOR = 0x000004,

		VF_UV0 = 0x000008,
		VF_UV1 = 0x000010,
		VF_UV2 = 0x000020,

		VF_UV0x = 0x001008,
		VF_UV1x = 0x001010,
		VF_UV2x = 0x001020,
	};

	enum ShaderFlag
	{
		POSTEX_SHADER = 1,
		POSTEXUC_SHADER,
		POSCLRTEXUC_SHADER,

		//POSCLR_SHADER,
		//POSCLRTEX_SHADER,
		//POSCLRNRM_SHADER,
		//POSNRMTEX_SHADER,
		//POSCLRNRMTEX_SHADER,

		BMFONT_SHADER,
	};

	class ShaderManager
	{
	public:
		static void init();
		static void quit();
		static ShaderManager& getInstance() { return *ShaderManager::_instance; }

		dry::Shader* getProgram(int shaderFlag);

		typedef std::map<GLuint, std::string> STRINGTABLE;

	private:
		static ShaderManager* _instance;
		
		STRINGTABLE _vertAttrTbl;
		STRINGTABLE _varyingTbl;

		std::map<int, dry::Shader*> _programTable;
		
		void initProgram();
		void releaseProgram();

		ShaderManager();
		virtual ~ShaderManager();
	};
}

#endif