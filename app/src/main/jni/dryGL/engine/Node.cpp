#include "Node.hpp"
#include <glm/gtc/type_ptr.hpp>

namespace OGL
{
	void BaseNode::attach(BaseNode* node)
	{
		// _ASSERT(node->_parent == nullptr);

		node->_parent = this;
		_children.push_back(node);
	}

	void BaseNode::dettach(BaseNode* node)
	{
		// _ASSERT(node->_parent == this);
		
		for (size_t i = 0; i < _children.size(); ++i)
		{
			if (_children[i] == node)
			{
				_children.erase(_children.begin() + i);
				node->_parent = nullptr;

				break;
			}
		}
	}

	void BaseNode::dettachFromParent()
	{
		if (_parent)
		{
			_parent->dettach(this);
		}
	}

	void RenderNode::updateTransform()
	{
		//_localTransform = glm::scale(_tscale) * glm::mat4_cast(_trotation) * glm::translate(_tposition);
		_localTransform = glm::translate(_tposition) * glm::mat4_cast(_trotation) * glm::scale(_tscale);
	}
}

