//
//  Image.h
//  dryGL
//
//  Created by Jordi Ros on 15/02/13.
//  Copyright (c) 2013 Jordi Ros. All rights reserved.
//

#ifndef DRY_GRAPHICS_IMAGE_H_
#define DRY_GRAPHICS_IMAGE_H_

namespace dry {
    
class Image
{
public:
                Image               ()
					: m_Data(NULL)
					, m_Width(0), m_Height(0)
					, m_Orientation(1)
				{}
               ~Image               () { Free(); }

    bool        InitWithData        (int width, int height, PixelFormat format, void const *data);
	bool		InitWithAsset		(const char* filename);
	bool		InitWithFile		(const char* filename);
	bool		InitWithImage		(int width, int height, Image& image, int sx = 0, int sy = 0);
    void        Free                ();

    int         GetWidth            () const { return m_Width;  }
    int         GetHeight           () const { return m_Height; }
    PixelFormat GetFormat           () const { return m_Format; }
    uchar      *GetData             () const { return m_Data;   }
    int         GetBPP              () const;
	int			GetOrientation		() const { return m_Orientation; }

	void		CopyFromImage		(Image& image, int sx, int sy);
	void		Clear(unsigned char b);
	void		GradientClear();

private:
    
    PixelFormat GetFormat           (int bpp) const;

private:
    
    int         m_Width;
    int         m_Height;
    PixelFormat m_Format;
	int			m_Orientation;

    uchar      *m_Data;
};

}

#endif
