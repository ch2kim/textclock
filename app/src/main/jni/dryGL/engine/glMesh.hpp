#ifndef GLMESH_HPP
#define GLMESH_HPP

#include "dry.h"
#include <base/StdMacro.hpp>

#include <vector>

namespace OGL
{
	template <typename T>
	struct GLMesh
	{
		std::vector<T> _vertexArray;
		std::vector<GLushort> _indexArray;

		size_t stride() { return sizeof(T); }
		
		void draw(GLenum mode = GL_TRIANGLES) 
		{
			glDrawElements(mode, (GLsizei)_indexArray.size(), GL_UNSIGNED_SHORT, &_indexArray[0]);
		}

		void clear()
		{
			_vertexArray.clear();
			_indexArray.clear();
		}
	};
};

#endif