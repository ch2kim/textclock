#include "DataBuffer.h"

namespace OGL
{
	DataBuffer::DataBuffer()
		: _buffer(NULL)
		, _size(0)
	{

	}

	DataBuffer::DataBuffer(long size)
	{
		_buffer = NULL;
		resize(size, true);
	}

	DataBuffer::~DataBuffer()
	{
		clear();
	}
	
	void DataBuffer::set(void* buf, long size)
	{
		clear();

		_buffer = buf;
		_size = size;
	}

	void DataBuffer::resize(long size, bool clear /* = false */)
	{
		if (_buffer)
			free(_buffer);

		_buffer = malloc(size);
		_size = size;

		if (clear)
			memset(_buffer, 0, _size);
	}
};